var express = require('express');
var app = express();

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

app.set('port', process.env.PORT || 33033);

var clients = [];

io.on('connection', function(socket){

	var player;

	console.log('Client ' + socket.id + ' connected');

	player = {id:socket.id}


	socket.on('PLAYER_START', function(data){

		player = {
			id:socket.id,
			name:data.name,
			position:data.position,
			angle:data.angle
		}

		clients[socket.id] = player;

		socket.emit('PLAYER_START', player);
		socket.broadcast.emit('PLAYER_CONNECTED', player);

		console.log('Player ' + player.name + ' joined, position: ' 
			+ player.position.x + ',' + player.position.y
			+ ',' + player.angle + 'deg');

	});

	socket.on('PLAYER_DISCONNECT', function(){

		playerDisconnect();

	});

	socket.on('MOVE', function(data){

		player.position = data.position;
		player.angle = data.angle;

		socket.broadcast.emit('MOVE', player);

		//console.log(player.name + ' move to ' 
			// + player.position.x + ',' + player.position.y
			// + ' rotate to ' + player.angle);

	});

	socket.on('WORLD_UPDATE', function(){
		EmitPlayersConnected();
	});

	socket.on('disconnect', function(){

		playerDisconnect();

	});

	function playerDisconnect(){

		socket.broadcast.emit('PLAYER_DISCONNECTED', player);
		console.log('Client ' + socket.id + ' disconnected');

	}

	function EmitPlayersConnected(){

		console.log(clients);

		for (var i=0; i<clients.length; i++){
			var playersConnected = {
				id:clients[i].id,
				name:clients[i].name,
				position:clients[i].position
			}

			socket.emit('PLAYER_CONNECTED', playersConnected);
		}

	}
});

server.listen(app.get('port'), function(){
	console.log('-------------- SERVER IS RUNNING --------------');
;})

app.get('/', function(req, res){
	res.send('<h1>Hello world. Welcome to Kite.io!</h1>');
});